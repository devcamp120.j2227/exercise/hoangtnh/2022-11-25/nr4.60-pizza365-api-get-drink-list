// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

// Import drink middleware
const drinkMiddleware = require("../middleWares/drinkMiddleware");

// Import drink controller
const drinkController = require("../controllers/drinkController");

router.get("/devcamp-pizza365/drinks", drinkController.getAllDrink);

router.post("/drinks", drinkController.createDrink);

router.get("/drinks/:drinkId", drinkMiddleware.getDetailDrinkMiddleware, drinkController.getDrinkById);
router.put("/drinks/:drinkId", drinkMiddleware.updateDrinkMiddleware, drinkController.updateDrinkById);
router.delete("/drinks/:drinkId", drinkMiddleware.deleteDrinkMiddlewar, drinkController.deleteDrinkById)

module.exports = router;