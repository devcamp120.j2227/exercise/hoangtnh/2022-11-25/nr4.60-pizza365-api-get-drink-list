//Import thư viện Mongoose
const mongoose = require("mongoose");
//Import Drink model
const drinkModel = require("../model/drinkModel");

//function Create Drink
const createDrink = (request, response) =>{
    // B1: Chuẩn bị dữ liệu
    const body = request.body;
    console.log(body);
    // B2: Validate dữ liệu
    //kiểm tra mã nước uống có được điền hay không
    if(!body.maNuocUong) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Mã nước uống không hợp lệ"
        })
    }
    if(!body.tenNuocUong) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Tên nước uống không hợp lệ"
        })
    }
    // Kiểm tra đơn giá có hợp lệ hay không
    if(isNaN(body.donGia) || body.donGia < 0) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Đơn giá không hợp lệ"
        })
    }
    // B3: Gọi Model tạo dữ liệu
    const newDrink = {
        _id: mongoose.Types.ObjectId(),
        maNuocUong: body.maNuocUong,
        tenNuocUong: body.tenNuocUong,
        donGia: body.donGia
    }

    drinkModel.create(newDrink, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(201).json({
            status: "Create new drink successfully",
            data: data
        })
    })
}

//function get all drink
const getAllDrink = (request, response) =>{
    // B1: Chuẩn bị dữ liệu
    // B2: Validate dữ liệu
    // B3: Gọi Model tạo dữ liệu
    drinkModel.find((error,data) =>{
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Get all drink successfully",
            data: data
        })
    })
}

//function get drink by Id
const getDrinkById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const drinkId = request.params.drinkId;
    
    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(drinkId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "drinkId không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    drinkModel.findById(drinkId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Get detail drink successfully",
            data: data
        })
    })
}
//function updateDrinkById
const updateDrinkById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const drinkId = request.params.drinkId;
    const body = request.body;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(drinkId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Drink Id không hợp lệ"
        })
    }

    if(body.maNuocUong !== undefined && body.maNuocUong.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "Mã nước uống không hợp lệ"
        })
    }

    if(body.tenNuocUong !== undefined && body.tenNuocUong.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "Tên nước uống không hợp lệ"
        })
    }

    if(body.donGia !== undefined && ( isNaN(body.donGia) || body.donGia < 0 )) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Đơn giá không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    const updateDrink = {}

    if(body.maNuocUong !== undefined) {
        updateDrink.maNuocUong = body.maNuocUong
    }

    if(body.tenNuocUong !== undefined) {
        updateDrink.tenNuocUong = body.tenNuocUong
    }

    if(body.donGia !== undefined) {
        updateDrink.donGia = body.donGia
    }

    drinkModel.findByIdAndUpdate(drinkId, updateDrink, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Update drink successfully",
            data: data
        })
    })
}

//function deleteDrinkById
const deleteDrinkById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const drinkId = request.params.drinkId;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(drinkId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "drinkId không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    drinkModel.findByIdAndDelete(drinkId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Delete drink successfully"
        })
    })
}
module.exports = {
    createDrink,
    getAllDrink,
    getDrinkById,
    updateDrinkById,
    deleteDrinkById
}