//import thư viện mongoose
const mongoose = require("mongoose");
//import order model
const orderModel = require("../model/orderModel");
//import user model
const userModel = require("../model/userModel");
//create order then push to user
const createOrder = (request, response) =>{
    //B1: chuẩn bị dữ liệu
    const body = request.body;
    const useremail = request.query.useremail;
    // const fullName = request.body.fullName;
    // const email = request.body.email;
    // const address = request.body.address;
    // const phone = request.body.phone;
    //B2: validate dữ liệu

    //B3: thao tác với CSDL
                    //sử dụng email user để tìm kiếm
                    userModel.findOne({email: useremail}, (errorEmail, emailExist) => {
                        if (errorEmail) {
                            return response.status(500).json({
                                status: "Error 500: Internal server error",
                                message: errorEmail.message
                            })
                        } else {
                            //nếu email không tồn tại
                            //tạo user mới
                                var fullname = Array("User1","User2","User3","User4");
                                const fullName = fullname[Math.floor(Math.random()*fullname.length)];
                                var addRess = Array("area1","area2","area3");
                                const address = addRess[Math.floor(Math.random()*addRess.length)];
                                var Phone = Array("0910231312","2342424","45645645");
                                const phone = Phone[Math.floor(Math.random()*Phone.length)];
                            userModel.create({
                                _id: mongoose.Types.ObjectId(),
                                fullName: fullName,
                                email: useremail,
                                address: address,
                                phone: phone
                                
                            }, (errCreateUser, userCreated) =>{
                                if (errCreateUser) {
                                    return response.status(500).json({
                                        status: "Email đã tồn tại",
                                        message: errCreateUser.message
                                    })
                                }
                                // return response.status(201).json({
                                //     status: "Create new user successfull",
                                //     data: userCreated
                                // })
                                //tạo order mới
                                var vPizzaSize = Array("S","M","L");
                                const randomPizzaSize = vPizzaSize[Math.floor(Math.random()*vPizzaSize.length)];
                                var vPizzaType = Array("Seafood","Bacon","Hawaii");
                                const randomPizzaType = vPizzaType[Math.floor(Math.random()*vPizzaType.length)];
                                orderModel.create({
                                    _id: mongoose.Types.ObjectId(),
                                    orderCode: mongoose.Types.ObjectId(),
                                    pizzaSize: randomPizzaSize,
                                    pizzaType: randomPizzaType,
                                    voucher: mongoose.Types.ObjectId(),
                                    status: "Open"
                                }, (error,data) =>{
                                    if(error){
                                        return response.status(500).json({
                                            status:"Internal server error",
                                            message: error.message
                                        })
                                    }
                                                //đẩy orderCode vào orders của user
                                            userModel.findOneAndUpdate({email: useremail}, {
                                                $push: {
                                                    orders: data._id
                                                }
                                            },(err, updateUser) =>{
                                                    if(err){
                                                        return response.status(500).json({
                                                            status: "Internal server error",
                                                            message: err.message
                                                        })
                                                    }
                                                        return response.status(201).json({
                                                            status:"Create order successfully",
                                                            data: data
                                                    })
                                            })
                                })
                            })
                        } if(emailExist) {
                            //nếu user email đã tồn tại
                            //tạo order mới
                            var vPizzaSize = Array("S","M","L");
                            const randomPizzaSize = vPizzaSize[Math.floor(Math.random()*vPizzaSize.length)];
                            var vPizzaType = Array("Seafood","Bacon","Hawaii");
                            const randomPizzaType = vPizzaType[Math.floor(Math.random()*vPizzaType.length)];
                            
                            orderModel.create({
                                _id: mongoose.Types.ObjectId(),
                                orderCode: mongoose.Types.ObjectId(),
                                pizzaSize: randomPizzaSize,
                                pizzaType: randomPizzaType,
                                voucher: mongoose.Types.ObjectId(),
                                status: "Open"
                            }, (error,data) =>{
                                if(error){
                                    return response.status(500).json({
                                        status:"Internal server error",
                                        message: error.message
                                    })
                                }
                                //đẩy orderCode vào orders của user
                            userModel.findOneAndUpdate({email: useremail}, {
                                $push: {
                                    orders: data._id
                                }
                            },(err, updateUser) =>{
                                    if(err){
                                        return response.status(500).json({
                                            status: "Internal server error",
                                            message: err.message
                                        })
                                    }
                                        return response.status(201).json({
                                            status:"Create order with exist email successfully",
                                            data: data
                                        })
                                    })
                                })
                            
                        }
                    })
                
}
const getUser = (request, response) => {
    userModel.find((error, data)=>{
        if(error){
            return response.status(400).json({
                status:"Bad request",
                message: error.message
            })
        }
        return response.status(200).json({
            status:"Get all user successfull",
            data: data
        })
    })
}
const getOrder = (request, response) => {
    orderModel.find((error, data)=>{
        if(error){
            return response.status(400).json({
                status:"Bad request",
                message: error.message
            })
        }
        return response.status(200).json({
            status:"Get all order successfull",
            data: data
        })
    })
}

module.exports = {
    createOrder,
    getUser,
    getOrder
}